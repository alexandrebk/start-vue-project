# START VUE PROJECT

###### powered by
[![Magor](https://cldup.com/5Mx8Oituad.png)](http://magor.com.br)

>Esse é um projeto em [node.js] com o Core em [Vue].

### Tecnologias
As Bibliotecas em uso são:
[Vue], [Webpack], [Twitter Bootstrap], [Jquery], [Gulp]

### Rodar
Instale as dependências e inicie o servidor

```sh
$ git clone https://allanferreira@bitbucket.org/allanferreira/start-vue-project.git
$ cd start-vue-project
$ npm i
$ npm run dev
```

Para o ambiente de produção use o `script build` para gerar os arquivos finais
e `script deploy` para gerar a pasta para colocar no servidor.

```sh
$ npm run build
$ npm run deploy
```

### License
MIT

[node.js]: <http://nodejs.org>
[Jquery]: <https://jquery.com/>
[Twitter Bootstrap]: <http://twitter.github.com/bootstrap>
[Webpack]: <https://webpack.js.org/>
[Vue]: <https://vuejs.org>
[Gulp]: <http://gulpjs.com/>
